package com.epam.kirill_barykin;


import screens.yandexMarketMain.ConstructOfMarketPage;
import screens.yandexMain.ConstructOfMainPage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class YandexTest {

    public String lowestPriceForSamsung = "40000";
    public String lowestPriceForBeats = "17000";
    public String highestPriceForBeats = "25000";
    String firstPhoneName;
    String firstPhoneTitle;
    String firstHeadphonesName;
    String firstHeadphonesTitle;

    private static WebDriver driver;
    public static ConstructOfMainPage constructOfMainPage;
    public static ConstructOfMarketPage constructOfMarketPage;
    CountDownLatch timer = new CountDownLatch(1);

    @BeforeClass
    public static void setup() {
        /* # Можно испольвать в "System.setProperty" параметр "chromedriver.exe" для запуска на Windows
        или "chromedriver" для запуска на Nix-системах */

        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();
        constructOfMainPage = new ConstructOfMainPage(driver);
        constructOfMarketPage = new ConstructOfMarketPage(driver);
        //driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://yandex.ru/");
        constructOfMainPage.marketButtonClick();
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }

    @Test
    public void phoneTitleCheck() throws InterruptedException {
        constructOfMarketPage.electronikaButtonClick();
        constructOfMarketPage.mobilePhoneButtonClick();
        constructOfMarketPage.samsungCheckBoxClick();
        constructOfMarketPage.setLowestPrice(lowestPriceForSamsung);
        String firstPhoneName = constructOfMarketPage.firstElementRemember();
        constructOfMarketPage.firstElementClick();
        String firstPhoneTitle = constructOfMarketPage.firstElementTitleLook();
        constructOfMarketPage.isTitleRight(firstPhoneName, firstPhoneTitle);
    }

    @Test
    public void headphonesTitleCheck() throws InterruptedException {
        constructOfMarketPage.electronikaButtonClick();
        constructOfMarketPage.naushnikiButtonClick();
        constructOfMarketPage.beatsCheckBoxClick();
        constructOfMarketPage.setLowestPrice(lowestPriceForBeats);
        constructOfMarketPage.setHighestPrice(highestPriceForBeats);
        String firstHeadphonesName = constructOfMarketPage.firstElementRemember();
        constructOfMarketPage.firstElementClick();
        String firstHeadphonesTitle = constructOfMarketPage.firstElementTitleLook();
        constructOfMarketPage.isTitleRight(firstHeadphonesName, firstHeadphonesTitle);
    }

    @After
    public void goToMain() throws InterruptedException {
        constructOfMarketPage.logoClick();
        constructOfMainPage.marketButtonClick();
    }
}
