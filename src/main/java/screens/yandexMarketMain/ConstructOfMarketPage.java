package screens.yandexMarketMain;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConstructOfMarketPage {
    public WebDriver driver;


    public ConstructOfMarketPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "//div/a/span[text() = 'Электроника']")
    private WebElement electronikaButton;

    @FindBy(xpath = "//div/a[text() = 'Мобильные телефоны']")
    private WebElement mobilePhoneButton;

    @FindBy(xpath = "//div/fieldset//li//div/span[text() = 'Samsung']")
    private WebElement samsungCheckBox;

    @FindBy(id = "glpricefrom")
    private WebElement lowestPrice;

    @FindBy(id = "glpriceto")
    private WebElement highestPrice;

    @FindBy(xpath = "//div[contains(@class, 'snippet-list')]/div[position()=1 and contains(@class, 'snippet-cell')]//div[contains(@class, 'title')]/a[contains(@class, 'link')]")
    private WebElement elementTitleInList;

    @FindBy(xpath = "//h1[contains(@class, 'title')]")
    private WebElement elementTitle;

    @FindBy(xpath = "//a[@class = 'logo logo_type_link logo_part_yandex']")
    private WebElement logo;

    @FindBy(xpath = "//div/a[text() = 'Наушники и Bluetooth-гарнитуры']")
    private WebElement headPhonesButton;

    @FindBy(xpath = "//div/fieldset//li//div/span[text() = 'Beats']")
    private WebElement beatsCheckBox;


    public void electronikaButtonClick() {
        electronikaButton.click();
    }

    public void mobilePhoneButtonClick() {
        mobilePhoneButton.click();
    }

    public void samsungCheckBoxClick() {
        samsungCheckBox.click();
    }

    public void setLowestPrice(String key) {
        lowestPrice.sendKeys(key);
    }

    public void setHighestPrice(String key) {
        highestPrice.sendKeys(key);
    }

    public String firstElementRemember() {
        String firstElementText = elementTitleInList.getText();
        return firstElementText;
    }

    public String firstElementTitleLook() {
        String firstElementTitle = elementTitle.getText();
        return firstElementTitle;
    }

    public void firstElementClick() {
        elementTitleInList.click();
    }

    public void logoClick() {
        logo.click();
    }

    public void isTitleRight(String a, String b) {

        Assert.assertEquals(a, b);
    }

    public void naushnikiButtonClick() {
        headPhonesButton.click();
    }

    public void beatsCheckBoxClick() {
        beatsCheckBox.click();
    }

}
