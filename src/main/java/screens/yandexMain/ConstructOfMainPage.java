package screens.yandexMain;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConstructOfMainPage {

    public WebDriver driver;

    public ConstructOfMainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "//a[text() = 'Маркет']")
    private WebElement marketButton;

    public void marketButtonClick() {
        marketButton.click();
    }
}
